# recipe to create the nomad-remote-tools-hub apmtools container via a dockerfile
FROM gitlab-registry.mpcdf.mpg.de/nomad-lab/nomad-remote-tools-hub/webtop:v0.0.1

# for testing the container locally without running in oauth errors
# FROM ghcr.io/linuxserver/webtop:amd64-ubuntu-openbox-version-e1079163
# # found that newest system python is 3.8.something
# # rest should come only from and be managed through (mini)conda not pip!
# # start of borrowed from gitlabmpcdf customized webtop
# ENV CUSTOM_PORT=8888
# ENV PUID=1000
# ENV PGID=1000
# # ports and volumes
# EXPOSE 8888
# VOLUME /config
# # end of borrowed from north webtop

USER root

RUN mkdir -p /home \
  && mkdir -p /home/atom_probe_tools \
  && mkdir -p /home/atom_probe_tools/apav \
  && mkdir -p /home/atom_probe_tools/aptyzer

COPY Cheatsheet.ipynb FAIRmatNewLogo.png NOMADOasisLogo.png /home/atom_probe_tools/
COPY APAVRunTestSuite.ipynb /home/atom_probe_tools/apav/
ENV PATH=/usr/local/miniconda3/bin:/home/atom_probe_tools/paraprobe-toolbox/code/thirdparty/mandatory/cmake/cmake-3.26.3/localinstallation/bin:$PATH

# query version afterwards like so dpkg -s <packagename> | grep '^Version:'
# OS dependencies, low-level system libraries, --fix-missing, rm -rf /var/lib/apt/lists/*
# future improvements should use --no-install-recommends to safe space further, but for mpich this created problems when building paraprobe-toolbox

RUN cd ~ \
  && apt update \
  && apt install -y curl \
  && curl -sL https://deb.nodesource.com/setup_18.x -o nodesource_setup.sh \
  && chmod +x nodesource_setup.sh \
  && ./nodesource_setup.sh \
  && apt install -y nodejs \
  && apt install -y m4 file git wget libgl1-mesa-dev libglu1-mesa-dev build-essential mpich libgmp-dev libmpfr-dev libssl-dev hwloc \
  && wget https://repo.anaconda.com/miniconda/Miniconda3-py38_23.1.0-1-Linux-x86_64.sh \
  && mv Miniconda3-py38_23.1.0-1-Linux-x86_64.sh miniconda3-py38_23.1.0-1-Linux-x86_64.sh \
  && chmod +x miniconda3-py38_23.1.0-1-Linux-x86_64.sh \
  && bash ./miniconda3-py38_23.1.0-1-Linux-x86_64.sh -b -p /usr/local/miniconda3 \
  && rm -f miniconda3-py38_23.1.0-1-Linux-x86_64.sh \
  && cd /home \
  && conda config --add channels conda-forge \
  && conda config --set channel_priority strict \
  && conda install -c conda-forge nomkl=1.0 jupyterlab-h5web=7.0.0 jupyterlab=3.6.3 hdbscan=0.8.29 gitpython=3.1.31 nodejs=18.15.0 ase=3.19.0 radioactivedecay=0.4.17 pandas=2.0.0 sphinx=6.1.3 \
  && conda clean -afy \
  && cd /home \
  && python3 -m pip install --upgrade pip \
  && python3 -m pip install ecdf==0.7.0 pytictoc==1.5.2 ifes-apt-tc-data-modeling==0.0.8 python-docs-theme==2023.3.1 \
  && cd /home \
  && cd /home/atom_probe_tools/apav \
  && python3 -m pip install apav[dev]==1.4.0 \
  && cd /home/atom_probe_tools \
  && git clone https://github.com/areichm/APTyzer.git \
  && cd APTyzer \
  && git checkout 39ab5e44c60f6f9e1c517f76b04914ac59e98bef \
  && cp APTyzer_V_1_2o.ipynb /home/atom_probe_tools/aptyzer/aptyzer.ipynb \
  && cp LICENSE /home/atom_probe_tools/aptyzer/LICENSE \
  && cp README.md /home/atom_probe_tools/aptyzer/README.md \
  && cp Tutorial_APTyzer_V1_2.pdf /home/atom_probe_tools/aptyzer/tutorial.pdf \
  && rm -rf /home/atom_probe_tools/APTyzer \
  && cd /home/atom_probe_tools \
  && git clone https://gitlab.com/paraprobe/paraprobe-toolbox.git \
  && cd paraprobe-toolbox \
  && git checkout e349fd34d7cfcb4282c2f3f61c69f9e5659f68e6 \
  && cd /home/atom_probe_tools/paraprobe-toolbox/code/thirdparty/mandatory \
  && mkdir -p cmake \
  && cd cmake \
  && wget https://cmake.org/files/v3.26/cmake-3.26.3.tar.gz \
  && tar -xvf cmake-3.26.3.tar.gz \
  && rm -rf cmake-3.26.3.tar.gz \
  && cd cmake-3.26.3 \
  && mkdir -p localinstallation \
  && chmod +x bootstrap \
  && ./bootstrap --prefix="$PWD/localinstallation/" -- -DCMAKE_USE_OPENSSL=OFF 2>&1 | tee PARAPROBE.CMake.Bootstrap.STDOUTERR.txt \
  && make -j4 2>&1 | tee PARAPROBE.CMake.Make4.STDOUTERR.txt \
  && make install 2>&1 | tee PARAPROBE.CMake.MakeInstall.STDOUTERR.txt \
  && cd /home/atom_probe_tools/paraprobe-toolbox/code/thirdparty/mandatory \
  && mkdir -p hdf5 \
  && cd hdf5 \
  && wget https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.14/hdf5-1.14.0/src/CMake-hdf5-1.14.0.tar.gz \
  && tar -xvf CMake-hdf5-1.14.0.tar.gz \
  && rm -rf CMake-hdf5-1.14.0.tar.gz \
  && cd CMake-hdf5-1.14.0 \
  && ./build-unix.sh 2>&1 | tee PARAPROBE.Hdf5.Build.STDOUTERR.txt \
  && ./HDF5-1.14.0-Linux.sh --include-subdir --skip-license 2>&1 | tee PARAPROBE.Hdf5.Install.STDOUTERR.txt \
  && cd /home/atom_probe_tools/paraprobe-toolbox/code/thirdparty/mandatory \
  && mkdir -p boost \
  && cd boost \
  && wget https://boostorg.jfrog.io/artifactory/main/release/1.81.0/source/boost_1_81_0.tar.gz \
  && tar -xvf boost_1_81_0.tar.gz \
  && rm -f boost_1_81_0.tar.gz \
  && cd /home/atom_probe_tools/paraprobe-toolbox/code/thirdparty/mandatory \
  && mkdir -p eigen \
  && cd eigen \
  && wget https://gitlab.com/libeigen/eigen/-/archive/3.4.0/eigen-3.4.0.tar.gz \
  && tar -xvf eigen-3.4.0.tar.gz \
  && rm -f eigen-3.4.0.tar.gz \
  && cd /home/atom_probe_tools/paraprobe-toolbox/code/thirdparty/mandatory \
  && mkdir -p cgal \
  && cd cgal \
  && wget https://github.com/CGAL/cgal/releases/download/v5.5.2/CGAL-5.5.2.tar.xz \
  && tar -xvf CGAL-5.5.2.tar.xz \
  && rm -f CGAL-5.5.2.tar.xz \
  && cd /home/atom_probe_tools/paraprobe-toolbox/code/thirdparty/mandatory \
  && mkdir -p voroxx \
  && cd voroxx \
  && git clone https://github.com/chr1shr/voro.git \
  && cd voro \
  && git checkout 56d619faf3479313399516ad71c32773c29be859 \
  && cd /home/atom_probe_tools/paraprobe-toolbox \
  && mkdir -p /home/atom_probe_tools/paraprobe-toolbox/teaching/example_data/usa_denton_smith \
  && mkdir -p /home/atom_probe_tools/paraprobe-toolbox/teaching/example_data/ger_erlangen_felfer \
  && cd /home/atom_probe_tools/paraprobe-toolbox \
  && sed -i 's|set(MYPROJECTPATH "<<YOURPATH>>/paraprobe-toolbox")|set(MYPROJECTPATH \"'"$PWD"'\/\")|g' code/PARAPROBE.Dependencies.cmake \
  && cd /home/atom_probe_tools/paraprobe-toolbox \
  && chmod +x PARAPROBE.Step05.Build.Tools.sh \
  && ./PARAPROBE.Step05.Build.Tools.sh \
  && chmod +x PARAPROBE.Step06.Install.Tools.sh \
  && ./PARAPROBE.Step06.Install.Tools.sh \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/code/thirdparty/mandatory/boost \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/code/thirdparty/mandatory/cgal \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/code/thirdparty/mandatory/chrono \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/code/thirdparty/mandatory/eigen \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/code/thirdparty/mandatory/hdf5 \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/code/thirdparty/mandatory/hornus2017 \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/code/thirdparty/mandatory/lewiner2002 \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/code/thirdparty/mandatory/voroxx \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/code/paraprobe-utils/build \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/code/paraprobe-ranger/build \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/code/paraprobe-selector/build \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/code/paraprobe-surfacer/build \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/code/paraprobe-distancer/build \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/code/paraprobe-tessellator/build \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/code/paraprobe-spatstat/build \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/code/paraprobe-nanochem/build \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/code/paraprobe-intersector/build \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/code/paraprobe-crystalstructure \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/code/paraprobe-miscellaneous \
  && chown -R ${PUID}:${PGID} /home \
  && chown -R ${PUID}:${PGID} /usr/local/miniconda3

# all rm -rf statements should only be used during container build when we do not like
# to have a version which can be recompiled inside the container
# as the entire C/C++ executables for all thirdparty dependencies are linked
# statically the entire thirdparty/mandatory is deleted during the container build
# which  freezes the state of each paraprobe tool but has the disadvantage that the
# tools can then not be recompiled from inside the container

COPY 02-exec-cmd /config/custom-cont-init.d/02-exec-cmd

ENV HOME=/home/atom_probe_tools
WORKDIR $HOME

# for running this container standalone e.g. docker run -p 3000:8888 <<imagename>>
# view in the browser e.g. via localhost:3000
