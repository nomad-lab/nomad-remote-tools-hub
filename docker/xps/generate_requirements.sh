#!/bin/bash
docker run --rm --env HOME=/home/jovyan --mount type=bind,source="$(pwd)",target=/home/jovyan \
            --entrypoint bash jupyter/scipy-notebook:2023-04-10 -c \
            "pip install pip-tools; pip-compile --resolver=backtracking --upgrade --output-file requirements.txt"