# NOMAD remote tools hub (NORTH) build files

This folder contains build + install instructions and examples for the NORTH analysis containers.
Each folder provides its own documentation, examples, `Dockerfile` and for some containers `docker-compose.yml` files
to run them locally.
