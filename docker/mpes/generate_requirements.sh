#!/bin/bash
docker run --rm --env HOME=/home/jovyan --mount type=bind,source="$(pwd)",target=/home/jovyan \
            --platform linux/amd64 --entrypoint bash jupyter/scipy-notebook:2023-04-10 -c \
            "pip install pip-tools; pip-compile --verbose --resolver=backtracking --upgrade --output-file requirements.jupyter.txt requirements.jupyter.in"

# This is not fully working yet!
# docker run --rm --env HOME=/config --mount type=bind,source="$(pwd)",target=/config/requirements \
#             --platform linux/amd64 --entrypoint bash gitlab-registry.mpcdf.mpg.de/nomad-lab/nomad-remote-tools-hub/webtop:v0.0.1 -c \
#             "apt-get update; apt-get install -y pip git; pip install pip-tools; cd requirements; pip-compile --verbose --output-file=requirements.webtop.txt --resolver=backtracking requirements.jupyter.in requirements.webtop.in"