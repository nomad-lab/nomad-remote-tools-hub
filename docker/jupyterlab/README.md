*** This is not used by the default NORTH configuration anymore. This moved to its own repo: https://gitlab.mpcdf.mpg.de/nomad-lab/north/jupyter ***

## JupyterLab

We are use a custom image based on [jupyter/datascience-notebook](https://hub.docker.com/r/jupyter/datascience-notebook).
We add the latest version of the `nomad-lab` Python package. The image is build by the
CI/CD and stored in the projects container registry.

To build the image

```
docker build -t gitlab-registry.mpcdf.mpg.de/nomad-lab/nomad-remote-tools-hub/jupyterlab:latest .
```

This folder stores the `Dockerfile` and a `run` command.
There are also Python named string parameters to make the cmd a bit more dynamic.
