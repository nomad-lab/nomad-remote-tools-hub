# STS example reader
***Note: Note: Though the reader name is STS reader it also supports STM experiment species. This is the first version of the reader according to the NeXus application definition NXsts which is a generic template of concepts' definition for STS and STM experiments. Later on, the application definitions and readers specific to the STM, STS and AFM will be available. To stay upto date keep visiting this page time to time. From now onwards we will mention STS referring both STM and STS.***
In this example, a few experimental example files are included.

Other two important files are [config_file_for_dat.json](config_file_for_dat.json) and [config_file_for_sxm.json](config_file_for_sxm.json) for STS and STM experiments respectively. These two files are intended to connect between raw data paths to application concepts of NXsts.nxdl.xml. To have a look on raw data path (organised inside the reader), one can use the following code that will generate corresponding output file:

```
from pynxtools.dataconverter.readers.sts import get_stm_raw_file_info
from pynxtools.dataconverter.readers.sts import get_sts_raw_file_info

# for stm (.sxm) file
get_stm_raw_file_info('STM_Example_file_1.sxm')

# for sts (.dat) file
get_sts_raw_file_info('STS_Example_file_1.dat')

```
To connect further the experiment data paths to the concepts and overwrite the [config_file_for_dat.json](config_file_for_dat.json) and the [config_file_for_sxm.json](config_file_for_sxm.json) one can take help of raw data explanation obtained from code above.

In this example folder other two important ELN files 1. [eln_data.yaml](eln_data.yaml)
and 2. [STS.scheme.archive.yaml](STS.scheme.archive.yaml). The ELN-1 will be needed to run the reader
from console or jupyter-notebook and ELN-2 to run the reader from Nomad software (Note that NOMAD 
software will generate an ELN named eln_data.yaml analogous to ELN-1 in an intermediate step.).

## Run STM reader 
STM reader can be run from jupyter notebook or console with the following code snippet

For stm experiment use the following code:
```
!dataconverter \
--reader sts \
--nxdl NXsts \
--input-file <PATH TO>/STM_nanonis_generic_4_5.sxm \
--input-file <PATH TO>/config_file_for_sxm.json \
--input-file <PATH TO>/eln_data.yaml \
--output stm_output.nxs
```

For sts experiment use the following code:
```
!dataconverter \
--reader sts \
--nxdl NXsts \
--input-file <PATH TO>/STS_nanonis_generic_5e_1.dat \
--input-file <PATH TO>/config_file_for_dat.json \
--input-file <PATH TO>/eln_data.yaml \
--output sts_output.nxs
```
***Note: To run STS and STM experiment from the same application definition NXsts, please edit experiment type as sts or stm***
For launching stm reader from jupyter notebook one can use 
[NotebookForSTM_STS_CONVERTER.ipynb](NotebookForSTM_STS_CONVERTER.ipynb) in this derectory.

## Supported Vendors and Versions
STM:
    Nanonis:
        versions: Generic 5e, Generic 4.5
STS:
    Nanonis:
        versions: Generic 5e, Generic 4.5
        
## OOPS
Still have issue write an issue in [pynxtools](https://github.com/FAIRmat-NFDI/pynxtools) or reach 
out the responsible person [@Rubel Mozumder](mozumder@physik.hu-berlin.de).
