FROM continuumio/miniconda3:4.10.3-alpine as builder

RUN /opt/conda/bin/conda install -c nion nionswift=0.16.3 nionswift-tool=0.4.14

USER root
RUN apk add --no-cache git && /opt/conda/bin/pip install git+https://git.physik.hu-berlin.de/kornilog/nionswift_frwr_plugin.git@4407db6b2d5e96be52f959f1cc84fc9861548b2f

RUN apk add --no-cache build-base
RUN /opt/conda/bin/pip install nionswift-experimental==0.7.8 nionswift-io==0.15.1 nionswift-instrumentation==0.20.6 nionswift-segmentation==0.0.3 nionswift-usim==0.4.3 nionswift-video-capture==0.2.3 nionswift-eels-analysis==0.6.1 nionswift-elabftw-plugin==0.1.2.4

FROM gitlab-registry.mpcdf.mpg.de/nomad-lab/nomad-remote-tools-hub/webtop:v0.0.1

COPY --from=builder /opt/conda/bin /opt/conda/bin
COPY --from=builder /opt/conda/lib /opt/conda/lib

# Add the command to run an app on startup
RUN echo "/opt/conda/bin/nionswift" > /defaults/autostart

# Limit/Modify the application menu (right click desktop) for the user
COPY menu.xml /defaults/menu.xml

COPY ./Nion /config/.local/share/Nion
COPY ./Documents /config/Documents
